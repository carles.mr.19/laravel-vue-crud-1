require('./bootstrap');

window.Vue = require('vue');

Vue.component('my-thougth-component', require('./components/MyThougthComponent.vue').default);
Vue.component('form-component', require('./components/FormComponent.vue').default);
Vue.component('thought-component', require('./components/ThoughtComponent.vue').default);

const app = new Vue({
    el: '#app',
});
